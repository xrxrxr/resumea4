function redirectMobileHandler() {
  const width = Math.max(document.clientWidth || 0, window.innerWidth || 0);
  if(width < 600) {
    window.location = 'https://xrxrxr.github.io/resume-template/';
  }
}

window.onload = redirectMobileHandler();
window.onresize = () => redirectMobileHandler();
